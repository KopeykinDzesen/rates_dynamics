
## RUN SCRIPT

1. `python3 -m venv venv | source ./venv/bin/activate`
2. `pip3 install -r requirements.txt`
3. `python3 main.py {currency} {start_date} {end_date}`
	пример: python3 main.py 145 1-1-2019 31-12-2019

Возможные *currency* указаны в файле currencies.py, также можно добавить и другие (тут http://www.nbrb.by/api/exrates/currencies нацбанк возвращает все возможные валюты с их **Cur_ID**)

*start_date* и *end_date* проверяются только на нужный формат, если указать что start_date больше чем end_date банк не вернёт данных и график будет пустой. также некоторых валют нету в базе, например статистика по RUB только до 2016 года, поэтому не удивляйся если будут пустые графики, возможно банк просто не присылает данных, это можно проверить послав запрос вручную http://www.nbrb.by/API/ExRates/Rates/Dynamics/{**Cur_ID**}?startDate={**start_date**}&endDate={**end_date**}. если будет пустой список, то это проблема со стороны банка.


---


## ОБЬЯСНЕНИЯ ПО КОДУ

старт программы функция main().

validate_args() проверяет валидность введённых с терминала аргументов. если они не валидны то выбрасывается исключение ValueError.
1. если их больше 4 (*main.py* тоже считается), 
2. если первый аргумент не совпадает ни с одним из ключей словаря `currencies/currencies`,
3. если второй и третий аргумент не соответствуют шаблону **%d-%m-%Y**

далее вызывается get_rates() из scraper.py
1. проверяет есть ли уже csv файл с данными, если есть просто выходит
2. если нету, то формируется ссылка к API нацбанка (можно здесь об этом почитать https://www.nbrb.by/APIHelp/ExRates), даты просто переворачиваются, т.к. нужен формат год-месяц-день, а мы по-другому вводили
3. safe_request() из utils.py делает зацикливание, это нужно потому что банк очень слабо отвечает, иногда вообще ошибки присылает, хотя запрос правильный, как на видео, если будешь преподу показывать, то лучше заранее запросы поделай, чтобы фалы csv загрузились, а потом всё быстро работало
4. safe_request() возвращает respons с json структурой, поэтому используется json.loads(), он преобразует json в словарь питона
5. далее с помощью библиотеки pandas создаётся DataFrame, сделано это только для того чтобы легко создать csv файл. (`[date_format(i["Date"]) for i in data]` это генераторы списков https://younglinux.info/python/feature/generators)
6. с помощью менеджера контекста (оператор with), создаётся и открывается файл, в который далее записывается dataframe
7. generate_file_path() просто генерирует уникальное имя файла, нужен для того чтобы в следующий раз не посылать запрос

возвращаемся в main.py
вызывается csv_parse(), который возвращает два списка, для x и для y
он проходит по файлу csv и построчно записывает все значения в списки
для простоты используется стандартная библиотека csv и csv.reader из неё
исключается первая строчка `first_line`, т.к. там по-умолчанию названия полей, а нам только данные нужны

`label` это подпись к графику

`plt.plot(x, y, "-", linewidth=1, label=label)`
с помощью matplotlib рисуется график, "-" это значит соединить точки линией, linewidth ширина линии

`plt.xticks(rotation=90)` повернуть надписи для оси x

`plt.xticks(shorten_dates(x))` так как надписей дат оочень много, может быть до 365, здесь говориться что нужно показывать только те что вернёт shorten_dates(x). он проходит по всему списку и оставляет каждый пятый элемент, остальные удаляет, если длина списка всё равно больше 31, то это повторяется.

`plt.legend()` добавляет надпись

`plt.show()` выводит на экран график

