import sys

from matplotlib import pyplot as plt
from utils import validate_args, shorten_dates, generate_file_path, csv_parse
from scraper import get_rates


def main():
    try:
        validate_args(sys.argv)
    except ValueError as e:
        print(e.args[0])
        return

    currency, start_date, end_date = sys.argv[1:]

    get_rates(currency, start_date, end_date)
    x, y = csv_parse(generate_file_path(currency, start_date, end_date))
    label = f"BYN/{currency} {start_date} - {end_date}"

    plt.plot(x, y, "-", linewidth=1, label=label)
    plt.xticks(rotation=90)
    plt.xticks(shorten_dates(x))
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
